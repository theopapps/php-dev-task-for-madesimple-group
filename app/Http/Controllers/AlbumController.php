<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Artist;
use Illuminate\Http\Request;
use Freshbitsweb\Laratables\Laratables;

class AlbumController extends Controller
{
    /**
     * Display the DataTable view
    */
    public function index()
    {
        $artists = Artist::orderBy('name')->get();
        return view('album.index', compact('artists'));
    }

    /**
     * Get the listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return Laratables::recordsOf(Album::class);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = Album::updateOrCreate(['id' => $request->id], $request->only(['name', 'artist_id', 'year']));
        return response()->json(['return' => $data ? true : false]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $data = Album::find($id);
        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(['return' => Album::destroy($id) > 0]);
    }
}
