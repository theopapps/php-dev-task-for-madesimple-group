<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'System Admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('admin')
        ]);
    }
}
