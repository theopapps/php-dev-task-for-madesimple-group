<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::resource('artist', 'ArtistController')->except(['create', 'edit'])->middleware('auth');
Route::resource('album', 'AlbumController')->except(['create', 'edit'])->middleware('auth');

Route::get('/artists', 'ArtistController@all')->middleware('auth');
Route::get('/albums', 'AlbumController@all')->middleware('auth');
