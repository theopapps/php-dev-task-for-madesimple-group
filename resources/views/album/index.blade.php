@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Albums</div>
                    <div class="panel-body">
                        <hr>
                        <p><button type="button" class="btn btn-primary" onClick="newRecord()">New Album</button></p>                        
                        <table class="table" id="recordDataTable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Artist</th>
                                    <th>Year</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="modalForm" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">/h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <input type="hidden" class="form-control" id="id" aria-describedby="id">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Enter the album name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                 <label for="name">Artist</label>
                                 <select id="artist_id" class="form-control">
                                    <option value="" readonly selected>Select...</option>
                                       @foreach($artists as $artist)
                                          <option value="{{ $artist->id }}">{{ $artist->name }}</option>
                                       @endforeach
                                 </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="year">Year</label>
                                <input type="text" class="form-control" id="year" aria-describedby="year" placeholder="Enter the album year">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onClick="saveRecord()" id="modalSubmitButton">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        var recordDataTable;

        $(document).ready(function () {
            recordDataTable = $('#recordDataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: 'albums',
                    type: 'GET',
                },
                columns: [
                    { name: 'name' },
                    { name: 'artist.name' },
                    { name: 'year' },
                    { 
                        data: 'id',
						name: 'id', 
						render: function ( data, type, row, meta ) { 
                            return '<a href="#" onClick="editRecord(' + row[3] + ')"><i class="fa fa-pencil"></i></a> | <a href="#" onClick="deleteRecord(' + row[3] + ', \'' + row[0] + '\')"><i class="fa fa-trash"></i></a>';
                        },  
                        orderable: false, 
                        searchable: false }
                ],
                order: [[0, 'desc']]
            });
        });

        // Shows the modal form to add a new record
        function newRecord(){
            clearFields();
            $("#modalTitle").html("New Album");
            $("#modalForm").modal();
        }

        function editRecord(id){
            $.ajax({
                type: 'GET',
                url: "/album/" + id,
                data: { 
                    '_token' : "{{ csrf_token() }}",
                    'id' : id 
                },
                success : function(response){
                    clearFields();
                    $("#modalTitle").html("Edit Album");
                    $("#id").val(response.data.id);
                    $("#name").val(response.data.name);
                    $("#artist_id").val(response.data.artist_id);
                    $("#year").val(response.data.year);
                    $("#modalForm").modal();
                }
            });
        }

        function saveRecord(){
            
            if(!validateFields()){
                alert("All the fields are required!");
                return;
            }

            $.ajax({
                type: 'POST',
                url: "album",
                data: { 
                    '_token' : "{{ csrf_token() }}",
                    'id': $("#id").val(),
                    'artist_id': $("#artist_id").val(),
                    'name': $("#name").val(),
                    'year': $("#year").val()
                },
                success : function(response){
                    if(response.return){
                        alert("Record Saved.");
                        $("#modalForm").modal('hide');
                        recordDataTable.ajax.reload();
                    }else{
                        alert("An error has occurred.");
                    }
                }
            });
        }

        function deleteRecord(id, name){
            if(confirm("Are you sure you want to delete the album " + name + " ?")){
                $.ajax({
                    type: 'DELETE',
                    url: "album/" + id,
                    data: { 
                        '_token' : "{{ csrf_token() }}",
                        'id': $("#id").val(),
                    },
                    success : function(response){
                        if(response.return){
                            alert("Record Deleted.");
                            recordDataTable.ajax.reload();
                        }else{
                            alert("An error has occurred.");
                        }
                    }
                });
            }
        }

        function validateFields(){            
            return !($("#name").val() === "" || $("#artist_id").val() === "" || $("#year").val() === "");
        }

        function clearFields(){
            $("#id").val("");
            $("#name").val("");
            $("#artist_id").val("");
            $("#year").val("");
        }
    </script>
@endsection
