<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $fillable = [ 'artist_id', 'name', 'year' ];

    public function artist()
    {
        return $this->belongsTo('App\Models\Artist');
    }
}
