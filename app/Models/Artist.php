<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artist extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $fillable = [ 'name', 'twitter_handle' ];
}
