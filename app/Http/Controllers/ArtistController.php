<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use Illuminate\Http\Request;
use Freshbitsweb\Laratables\Laratables;

class ArtistController extends Controller
{
    /**
     * Display the DataTable view
    */
    public function index()
    {
        return view('artist.index');
    }

    /**
     * Get the listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return Laratables::recordsOf(Artist::class);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $data = Artist::updateOrCreate(['id' => $request->id], $request->only(['name', 'twitter_handle']));
        return response()->json(['return' => $data ? true : false]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $data = Artist::find($id);
        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(['return' => Artist::destroy($id) > 0]);
    }
}
