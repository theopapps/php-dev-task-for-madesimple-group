# PHP Dev Task - madesimplegroup.com

This project is a task for a position @ MadeSimple Group

# Main Task

## Login
- Create a basic login page with the fields username and password
- Once logged in the user should be able to access all internal pages of the music app
- If a not-logged in user tries to access any internal page he should be redirected to the login page
- After a successful login the user should be redirected to the Artists list page
- A failed login attempt will send the user back to the login page with the error: Sorry, we couldn't find an account with this username. Please check you're using the right username and try again.

## Artists
- Create 3 pages that the will let users list, add and edit artists.
- Each artist will contain the following fields:
	- Artist name
	- Twitter handle

## Albums
- Create 3 pages that will let users list, add and edit albums.
- Each album will contain the following fields:
	- Artist (let user select from the list of existing artists)
	- Album name
	- Year
	
# Info

## About
- This app was made with Laravel Framework (https://laravel.com/) and using MVC architecture
- The CRUD was made using PHP and JavaScript (JQuery and Ajax)

## Deploy
- To deploy this app just follow this steps:
	- Clone this repository
	- Create de .env file (don't forget to set up a database connection)
	- Run these commands:
		- composer update (to update all packages)
		- php artisan key:generate (to generate the app key - if you don't have it)
		- php artisan migrate --seed (to migrate the database and seed)
	- To start the app run "php artisan serve" and a webserver will be started at 127.0.0.1:8000 (default port)

## Demo
- URL: http://task.ebio.com.br
	- user: admin@mail.com
	- pass: admin
	
Last update: 2019-08-14 @ 08:52